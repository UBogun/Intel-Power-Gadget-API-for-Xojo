#tag Module
Protected Module IPG
	#tag Method, Flags = &h1, Description = 54686520616476657274697365642070726F636573736F72206672657175656E637920666F7220746865207061636B6167652073706563696669656420627920694E6F64652E
		Protected Function BaseFrequency(node as int32 = 0) As double
		  Dim result As Double
		  Dim success As Boolean = GetBaseFrequency(node, result)
		  Return If (success, result, -1)
		End Function
	#tag EndMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetBaseFrequency Lib PowerGadgetFramework (node as int32, byref result as double) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetGTFrequency Lib PowerGadgetFramework (byref result as int32) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetIAFrequency Lib PowerGadgetFramework (node as int32, byref result as int32) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetMaxTemperature Lib PowerGadgetFramework (node as int32, byref result as int32) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetMsrFunc Lib PowerGadgetFramework (msr as int32, byref result as MSRFunction) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetMsrName Lib PowerGadgetFramework (msr as int32, result as ptr) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetNumMsrs Lib PowerGadgetFramework (byref nodes as int32) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetNumNodes Lib PowerGadgetFramework (byref nodes as int32) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetPowerData Lib PowerGadgetFramework (node as int32, msr as int32, dval as ptr, byref ival as int32) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetSysTime Lib PowerGadgetFramework (result as ptr) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetTDP Lib PowerGadgetFramework (node as int32, byref result as double) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetTemperature Lib PowerGadgetFramework (node as int32, byref result as int32) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetThresholds Lib PowerGadgetFramework (node as int32, byref result as int32, byref result1 as int32) As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function GetTimeInterval Lib PowerGadgetFramework (byref value as double) As Boolean
	#tag EndExternalMethod

	#tag Method, Flags = &h1, Description = 496620496E74656CC2AE20677261706869637320697320617661696C61626C65202E
		Protected Function GTAvailable() As Boolean
		  return IsGTAvailable
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 5468652063757272656E74204754206672657175656E637920696E204D487A2E
		Protected Function GTFrequency() As Int32
		  Dim result As Int32
		  Dim success As Boolean = getGTFrequency(result)
		  Return If (success, result, -1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 496E697469616C697A657320746865206672616D65776F726B2E
		Protected Function Initialize() As Boolean
		  return IntelEnergyLibInitialize
		End Function
	#tag EndMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function IntelEnergyLibInitialize Lib PowerGadgetFramework () As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function IsGTAvailable Lib PowerGadgetFramework () As Boolean
	#tag EndExternalMethod

	#tag Method, Flags = &h1, Description = 5265616473207468652074656D706572617475726520746172676574204D5352206F6E20746865207061636B6167652073706563696669656420627920694E6F64652C20616E642072657475726E7320746865206D6178696D756D2074656D706572617475726520696E20646567726565732043656C736975732E
		Protected Function MaxTemperature(node as int32 = 0) As Int32
		  Dim result As Int32
		  Dim success As Boolean = GetMaxTemperature(node, result)
		  Return If (success, result, -1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 5468652066756E6374696F6E206F662074686520737065636966696564204D53522E20
		Protected Function MSRFunction(MSR as int32) As MSRFunction
		  Dim result As MSRFunction
		  Dim success As Boolean = GetMsrFunc(msr, result)
		  Return If (success, result, MSRFunction(-1))
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 546865206E616D65206F662074686520737065636966696564204D53522E
		Protected Function MSRName(MSR as int32) As String
		  Dim result As New MemoryBlock(32) 
		  Dim success As Boolean = GetMsrName(msr, result)
		  Return If (success, result.CString(0), "")
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 546865206E756D626572206F66204D535273206F6E207468652073797374656D2E
		Protected Function MSRs() As Int32
		  Dim result As Int32
		  Dim success As Boolean = GetNumMsrs(result)
		  Return If (success, result, -1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 546865206E756D626572206F6620435055207061636B61676573206F6E207468652073797374656D2E
		Protected Function Nodes() As Int32
		  Dim result As Int32
		  Dim success As Boolean = GetNumNodes(result)
		  return if (success, result, -1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 526561647320746865207061636B61676520706F77657220696E666F204D5352206F6E20746865207061636B61676520737065636966696564206279204E6F64652C20616E642072657475726E7320746865205444502028696E205761747473292E204974206973207265636F6D6D656E6465642074686174205061636B61676520506F776572204C696D6974206973207573656420696E7374656164206F6620544450207768656E6576657220706F737369626C652C2061732069742069732061206D6F726520616363757261746520757070657220626F756E6420746F20746865207061636B61676520706F776572207468616E205444502E
		Protected Function PackagePower(node as int32 = 0) As Double
		  Dim result As Double
		  Dim success As Boolean = GetTDP(node, result)
		  Return If (success, result, -1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 52657475726E7320746865206461746120636F6C6C656374656420627920746865206D6F737420726563656E742063616C6C20746F205265616428292E205468652072657475726E6564206461746120697320666F72207468652064617461206F6E20746865207061636B61676520737065636966696564206279204E6F64652C2066726F6D20746865204D535220737065636966696564206279204D53522E207365652068747470733A2F2F736F6674776172652E696E74656C2E636F6D2F626C6F67732F323031342F30312F30372F7573696E672D7468652D696E74656C2D706F7765722D6761646765742D33302D6170692D6F6E2D77696E646F7773
		Protected Function PowerData(node as int32 = 0, msr as int32) As double()
		  Dim dresult() As Double
		  Dim buffer As New MemoryBlock(24)
		  Dim success As Boolean
		  Dim iresult As Int32
		  success = GetPowerData(node, msr, buffer, iresult)
		  If success Then
		    For q As Integer = 0 To iresult -1
		      dresult.Append buffer.DoubleValue(q*8)
		    Next
		    Return dresult
		  End If
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 5265616473207468652070726F636573736F72206672657175656E6379204D5352206F6E20746865207061636B61676520737065636966696564206279204E6F64652C20616E642072657475726E7320746865206672657175656E63792028696E204D487A292E
		Protected Function ProcessorFrequency(node as int32 = 0) As Int32
		  Dim result As Int32
		  Dim success As Boolean = GetIAFrequency(node, result)
		  Return If (success, result, -1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 52656164732073616D706C6520646174612066726F6D207468652064726976657220666F7220616C6C2074686520737570706F72746564204D5352732E
		Protected Function Read() As Boolean
		  return ReadSample
		End Function
	#tag EndMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function ReadSample Lib PowerGadgetFramework () As Boolean
	#tag EndExternalMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function StartLog Lib PowerGadgetFramework (filename as cstring) As Boolean
	#tag EndExternalMethod

	#tag Method, Flags = &h1, Description = 53746172747320736176696E6720746865206461746120636F6C6C65637465642062792052656164282920756E74696C2053746F704C6F6728292069732063616C6C65642E2043616C6C205265616428292072657065617465646C7920647572696E6720746869732074696D652E
		Protected Function StartLogging(File as FolderItem) As Boolean
		  return StartLog(file.NativePath)
		End Function
	#tag EndMethod

	#tag ExternalMethod, Flags = &h21
		Private Soft Declare Function StopLog Lib PowerGadgetFramework () As Boolean
	#tag EndExternalMethod

	#tag Method, Flags = &h1, Description = 53746F707320736176696E6720746865206461746120636F6C6C6563746564206279205265616428292E
		Protected Function StopLogging() As Boolean
		  return StopLog()
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 52657475726E73207468652073797374656D2074696D65206173206F6620746865206C6173742063616C6C20746F205265616453616D706C6528292E0A4E6F74653A205468697320636F6465206973206E6F74206F6B2E
		Protected Function SystemTime() As double
		  Dim result As Double
		  Dim buffer As New MemoryBlock(8)
		  Dim p As ptr
		  Dim success As Boolean = GetSysTime( buffer )
		  If success Then
		    result = buffer.Int32Value(4)+buffer.Int32Value(0)/1000000000
		    Return result
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 5265616473207468652074656D7065726174757265204D5352206F6E20746865207061636B61676520737065636966696564206279204E6F64652C20616E642072657475726E73207468652063757272656E742074656D706572617475726520696E20646567726565732043656C736975732E
		Protected Function Temperature(node as int32 = 0) As Int32
		  Dim result As Int32
		  Dim success As Boolean = GetTemperature(node, result)
		  Return If (success, result, -1)
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 556E646F63756D656E7465642066756E6374696F6E2C20696E2074657374732072657475726E696E67204D617854656D702074776963652E
		Protected Function TemperatureThresholds(node as int32 = 0) As Int32()
		  Dim result(), result1, result2 As Int32
		  Dim success As Boolean = GetThresholds(node, result1, result2)
		  If success Then 
		    Return Array(result1, result2)
		  Else
		    result.Append -1
		    result.Append -1
		    Return result
		  End If
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1, Description = 52657475726E73207468652074696D652028696E207365636F6E64732920746861742068617320656C6170736564206265747765656E207468652074776F206D6F737420726563656E742063616C6C7320746F205265616428292E
		Protected Function TimeInterval() As double
		  Dim result As Double
		  Dim success As Boolean = GetTimeInterval(result)
		  Return If (success, result, -1)
		End Function
	#tag EndMethod


	#tag Constant, Name = PowerGadgetFramework, Type = String, Dynamic = False, Default = \"IntelPowerGadget.framework", Scope = Private
	#tag EndConstant


	#tag Enum, Name = MSRFunction, Type = Int32, Flags = &h0
		Frequency = 0
		  Power = 1
		  Temperature = 2
		PackagePowerLimit = 3
	#tag EndEnum


	#tag ViewBehavior
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule
