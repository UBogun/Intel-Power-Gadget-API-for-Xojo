# Intel-Power-Gadget-API-for-Xojo

**Xojo declares into Intel Power Gadget API (currently Mac)**


![Demo Project](Screenshot.png)

**Usage** is simple:  
* Install the Power Gadget from https://software.intel.com/en-us/articles/intel-power-gadget-20.  
* Run the demo project.  

The project will list the number of CPU packages and the available sensor data with name and type.  
  
For own use, copy the IPG module into your own project.  
Start by initializing the framework with   
```
Dim success As Boolean = IPG.Initialize
```  
  
Many properties are readily accessible after initialization.  
For access to the MSRs, you have to perform a  
```
success = IPG.Read
```
first.  

Then you can read their data with  
```
Dim Result() As Double = IPG.PowerData(Node, MSR) 
```
Result will be an array of up to 3 doubles, depending on the MSR type you enquired.  
(Refer to the supplier’s documentation)  
  
You can even create a CSV with  
```
success = IPG.StartLogging(F As FolderItem) 
```
  
Every time you   
```
Call IPG.Read  
```
an entry in the CSV is created until you cast an  
```
success = IPG.StopLogging.  
```
  
>>>
The demo project does not show best practice. You should check the result first.   
Instead of returning a boolean for the query success, many functions return -1 values in case of failure.
>>>